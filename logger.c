#include "logger.h"
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define MAX_CHANNELS 1000
#define MAX_MSG_SIZE 255
#define BUFFER_SIZE 1000

FILE* file;

int log_level = 0;

char** message_buffer;
int next_allocatable_message;

char** channels;
int next_allocatable_channel;

pthread_mutex_t write_to_buffer = PTHREAD_MUTEX_INITIALIZER;

void write_to_file(char** buffer, int msg_count) {
	if (file != NULL) {
		for (int i = 0; i < msg_count; i++) {
			fprintf(file, "%s\n", buffer[i]);
		}
	}
	next_allocatable_message = 0;
}

void logger_initialize(FILE *f) {
	file = f;	
	channels = malloc(MAX_CHANNELS * sizeof(char*));
	message_buffer = malloc(BUFFER_SIZE * sizeof(char*));
}

void logger_finish() {
	write_to_file(message_buffer, next_allocatable_message);
	free(channels);
	free(message_buffer);
}

int logger_register_channel(char* name) {
	int c = next_allocatable_channel++;
	channels[c] = name;
	return c;
}

void logger_set_level(int level) {
	log_level = level;
}

char* parse_message(int channel, char* msg) {
	char* channel_name = channels[channel];
	char* result = malloc(strlen(channel_name) + strlen(msg) + 4);
	strcat(result, "[");
	strcat(result, channel_name);
	strcat(result, "]: ");
	strcat(result, msg);
	return result;
}

void write_log(int channel, char* msg) {
	char* result_message = parse_message(channel, msg);
	char** write_to_file_buffer;
	int write_to_file_count = -1;
	pthread_mutex_lock(&write_to_buffer);
	int pos = next_allocatable_message++;	
	if (message_buffer[pos] != NULL) {
		free(message_buffer[pos]);
	}
	message_buffer[pos] = malloc(strlen(result_message) + 1);
	strcpy(message_buffer[pos], result_message);
	if (next_allocatable_message == BUFFER_SIZE) {
		write_to_file_buffer = malloc(BUFFER_SIZE, sizeof(char*));
		memcpy(write_to_file_buffer, message_buffer, (BUFFER_SIZE * sizeof(char*)));
		write_to_file_count = next_allocatable_message;
	}
	pthread_mutex_unlock(&write_to_buffer);
	if (write_to_file_count > 0) {
		write_to_file(write_to_file_buffer, write_to_file_count);
	}
}

void logger_info(int channel, char* msg) {
	if (log_level >= 2) {
		write_log(channel, msg);
	}
}

void logger_warn(int channel, char* msg) {
	if (log_level >= 1) {
		write_log(channel, msg);
	}
}

void logger_error(int channel, char* msg) {
	if (log_level >= 0) {
		write_log(channel, msg);
	}
}

