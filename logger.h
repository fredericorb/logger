#ifndef LOGGER
#define LOGGER

#include <stdio.h>


void logger_initialize(FILE *f);

int logger_register_channel(char* name);

void logger_set_level(int level);

void logger_info(int channel, char* msg);

void logger_warn(int channel, char* msg);

void logger_error(int channel, char* msg);

void logger_finish();

#endif
