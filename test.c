#include "logger.h"
#include <stdlib.h>
#include <stdio.h>

int main() {
	FILE* f = fopen("log.txt", "w");
	logger_initialize(f);

	logger_set_level(2);

	int ch1 = logger_register_channel("thread 1");
	int ch2 = logger_register_channel("thread 2");
	int ch3 = logger_register_channel("thread 3");

	logger_info(ch1, "started running");
	logger_info(ch2, "started running");
	logger_warn(ch1, "undefined settings");
	logger_error(ch3, "failed to initialize");

	for (int i = 0; i < 1001; i++) {
		logger_info(ch1, "aaaaa");
	}

	logger_finish();
	fclose(f);
}
