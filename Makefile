CC=gcc
FLAGS= -Wall -g -lpthread

test: logger.o test.c
	$(CC) test.c logger.o $(FLAGS) -o test 

%.o: %.c
	$(CC) -c -o $@ $(FLAGS) $<
